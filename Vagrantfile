# -*- mode: ruby -*-
# vi: set ft=ruby :

github_url      = "https://raw.githubusercontent.com/fideloper/Vaprobash/1.0.0"

mysql_root_password   = "root"   # We'll assume user "root"
mysql_version         = "5.7"    # Options: 5.5 | 5.6
mysql_enable_remote   = "true"   # remote access enabled when true

hostname        = "sandbox.local"
server_ip       = "66.66.66.66"
server_memory   = "4096" # MB
server_swap     = "4096" # Options: false | int (MB) - Guideline: Between one or two times the server_memory
public_folder   = "/vagrant/www"
time_zone       = "UTC"

Vagrant.configure("2") do |config|
  config.vm.box = "generic/ubuntu1804"
  config.vm.hostname = hostname
  
  config.vm.network "private_network", ip: server_ip
  config.vm.network "forwarded_port", guest: 3306, host: 33306
  config.vm.network "forwarded_port", guest: 80, host: 8080
  config.vm.box_check_update = false
  config.vm.synced_folder ".", "/vagrant", nfs: false

  config.vm.provider "virtualbox" do |vb|
    vb.name = "vagrant_ubuntu1804"
    vb.customize ["modifyvm", :id, "--memory", server_memory] 
    vb.customize ["guestproperty", "set", :id, "/VirtualBox/GuestAdd/VBoxService/--timesync-set-threshold", 10000]
    vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
  end
  
  if Vagrant.has_plugin?("vagrant-cachier")
    # Configure cached packages to be shared between instances of the same base box.
    # Usage docs: http://fgrehm.viewdocs.io/vagrant-cachier/usage
    config.cache.scope = :box

    config.cache.synced_folder_opts = {
        type: :nfs,
        mount_options: ['rw', 'vers=3', 'tcp', 'nolock', 'fsc','actimeo=2']
    }
  end  
  
  $prescript = <<-SCRIPT    
    sudo apt-get -y install debconf-utils
    sudo echo '* libraries/restart-without-asking boolean true' | sudo debconf-set-selections
    sudo DEBIAN_FRONTEND=noninteractive apt-get install -y libpq-dev
    sudo apt-get -y install software-properties-common
    sudo add-apt-repository ppa:openjdk-r/ppa -y 
    sudo add-apt-repository ppa:ondrej/php -y
    sudo add-apt-repository -y ppa:ondrej/mysql-5.6 -y
    sudo apt-get -y update 
    sudo apt-get install -q -y resolvconf && sudo echo "nameserver 8.8.8.8" | sudo tee /etc/resolv.conf > /dev/null 2>&1
    sudo rm /etc/localtime && sudo ln -s /usr/share/zoneinfo/UTC /etc/localtime
  SCRIPT
  
  config.vm.provision "shell", inline: $prescript  
  config.vm.provision "shell", path: "#{github_url}/scripts/mysql.sh", args: [mysql_root_password, mysql_version, mysql_enable_remote]
  config.vm.provision "shell" do |s|
    s.args = [time_zone]
    s.inline = <<-SHELL
	sudo timedatectl set-timezone UTC

	sudo apt-get install curl
	sudo apt-get install -y nodejs
	sudo apt-get install -q -y nano git locate nginx curl git unzip composer 
	sudo apt-get install -q -y tk8.5 tcl8.5
	sudo apt-get install -q -y build-essential tcl
	sudo apt-get install -q -y redis-tools php-redis
	sudo apt-get install -q -y php7.4  
	sudo apt-get install -q -y php7.4-fpm 
	sudo apt-get install -q -y php7.4-cli   
	sudo apt-get install -q -y php7.4-common 
	sudo apt-get install -q -y php7.4-json
	sudo apt-get install -q -y php7.4-curl 
	sudo apt-get install -q -y php7.4-gd 
	sudo apt-get install -q -y php7.4-mbstring 
	sudo apt-get install -q -y php7.4-mysql 
	sudo apt-get install -q -y php7.4-xml 
	sudo apt-get install -q -y php7.4-zip 
	sudo apt-get install -q -y php7.4-bz2 
	sudo apt-get install -q -y php7.4-intl
	sudo apt-get install -q -y php7.4-imagick 
	sudo apt-get install -q -y php7.4-recode 
	sudo apt-get install -q -y php7.4-tidy 
	sudo apt-get install -q -y php7.4-xmlrpc 
	sudo apt-get remove  -q -y apache2 &>/dev/null
	sudo apt-get install -q -y build-essential tcl

	echo "127.0.0.1 sandbox.dev" | sudo tee /etc/hosts -a
	echo "127.0.0.1 sandbox.local" | sudo tee /etc/hosts -a

	sudo rm /etc/nginx/sites-enabled/vagrant > /dev/null 2>&1
	sudo cp /vagrant/default /etc/nginx/sites-enabled/default
	sudo service nginx restart
	sudo service php7.4-fpm restart
	sudo update-alternatives --set php /usr/bin/php7.4

	sudo composer config --global disable-tls true
	sudo composer config --global secure-http false

	sudo mysql -u root -proot -e 'DROP DATABASE IF EXISTS `sandbox`;' > /dev/null 2>&1
	sudo mysql -u root -proot -e 'CREATE DATABASE `sandbox`;' > /dev/null 2>&1

	echo "CREATE USER 'sandbox'@'%' IDENTIFIED BY 'sandbox'" | mysql -u root -proot > /dev/null 2>&1
	echo "GRANT ALL PRIVILEGES ON sandbox.* TO 'sandbox'@'%'" | mysql -u root -proot > /dev/null 2>&1

	sudo cp /vagrant/.env.local /vagrant/public_html/.env
	cd /vagrant/public_html
	php artisan migrate --seed
    SHELL
  end
end
